const multer = require('multer');
const mkdirp = require('mkdirp');
const fs = require('fs');

const getDirImage = (userID) => {
    let year = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    let day = new Date().getDay();
    if (userID != '') {
        return `./public/uploads/users/${userID != '' ? userID : 'unknown'}/images/${year}/${month}/${day}`;
    }else{
        return `./public/uploads/images/${year}/${month}/${day}`;
    }
}

const ImageStorage = multer.diskStorage({
    destination : (req , file , cb) => {
        // console.log(file);
        let dir = getDirImage(req.user.id);
        mkdirp.sync(dir);
        cb(null , dir)
    },
    filename : (req , file , cb) => {
        let filePath = getDirImage(req.user.id) + '/' + file.originalname;

        if(!fs.existsSync(filePath))
            cb(null , file.originalname);
        else
            cb(null , Date.now() + '_' + file.originalname);

    }
})

const imageFilter = (req , file , cb) => {
    // if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    //     return cb(new Error('Only image are allowed.'), false);
    // }
    // cb(null, true);
    if(file.mimetype === "image/png" || file.mimetype === "image/jpeg" || file.mimetype === "image/jpg") {
        cb(null , true)
    } else {
        cb(null , false)
    }
}
// const imageFilter = (req , file , cb) => {
//     if(file.mimetype === "image/png" || file.mimetype === "image/jpeg" || file.mimetype === 'video/mp4') {
//         cb(null , true)
//     } else {
//         cb(null , false)
//     }
// }
const uploadImage = multer({
    storage : ImageStorage,
    limits : {
    files: 1, // allow up to 1 files per request,
        fileSize : 1024 * 1024 * 10 // 10 MB (max file size)
    },
    fileFilter : imageFilter,
});

module.exports = uploadImage;