const multer = require('multer');
const mkdirp = require('mkdirp');
const fs = require('fs');

const getDirVideo = (userID) => {
    let year = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    let day = new Date().getDay();
    if (userID != '') {
        return `./public/uploads/users/${userID != '' ? userID : 'unknown'}/videos/${year}/${month}/${day}`;
    }else{
        return `./public/uploads/videos/${year}/${month}/${day}`;
    }
}

const VideoStorage = multer.diskStorage({
    destination : (req , file , cb) => {
        let dir = getDirVideo(req.user.id);
        mkdirp.sync(dir);
        cb(null , dir)
    },
    filename : (req , file , cb) => {
        let filePath = getDirVideo(req.user.id) + '/' + file.originalname;

        if(!fs.existsSync(filePath))
            cb(null , file.originalname);
        else
            cb(null , Date.now() + '_' + file.originalname);

    }
})

const videoFilter = (req , file , cb) => {
    if (!file.originalname.match(/\.(mp4|gif)$/)) {
        return cb(new Error('Only Video are allowed.'), false);
    }
    cb(null, true);
    // if(file.mimetype === "video/mp4" ) {
    //     cb(null , true)
    // } else {
    //     cb(null , false)
    // }
}

const uploadVideo = multer({
    storage : VideoStorage,
    limits : {
    files: 5, // allow up to 5 files per request,
        fileSize : 1024 * 1024 * 30 // 10 MB (max file size)
    },
    // fileFilter : videoFilter,
});

module.exports = uploadVideo;