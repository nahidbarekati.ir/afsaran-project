const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    title : { type : String , required : true} ,
    telegram : { type : String , required : true} ,
    position : { type : Number},
    mediaUrl : {  type : String  },
    mediaType : {  type : String  },
    mediaName : {  type : String  },
    mediaExtension : {  type : String  },
    media : { type : Schema.Types.ObjectId , ref : 'Media'},
    status :  [{ type : Schema.Types.ObjectId , ref : 'Status'}],
});

module.exports = mongoose.model('Category' , CategorySchema);