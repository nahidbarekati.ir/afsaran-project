const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const timestamp = require('mongoose-timestamp');


const MediaSchema = new Schema ({
    titre : { type : String , required : true},
    url : { type : String , required : true},
    type : { type : String , required : true},
    extension : { type : String , required : true},
})
 

MediaSchema.plugin(timestamp /*, {
    createdAt : created_at,
    updatedAt : updated_at
}*/)

module.exports = mongoose.model('Media' , MediaSchema);