const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');
const mongoosePaginate = require('mongoose-paginate');
// const datetime = require("jalali-moment");

const UserSchema = new Schema({
    username: {type: String},
    name: {type: String},
    lastname: {type: String},
    mobile: {type: String, required: true, unique: true},//V2
    code: {
        otp: {type: String, default: '',},
        expiredAt: {type: String, default: null},
        use: {type: Boolean, default: false,},
    },
    role: {type: String, default: 'guest'},
    email: {type: String, unique: true},
    password: {type: String},
    about: {type: String, default: null},
    avatar: {
        type: String,
        default: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSq06v_YeFtM-5YtnSiHIP1vqUsYva3WqKPmXNzb_tpCzwk6E6W'
    },//v2
    social: {type: JSON, default: {facebook: null, twitter: null, youtube: null, instagram: null, telegram: null}},//v2
    status: [{type: Schema.Types.ObjectId, ref: 'Status'}], //v2
    lastLogin: {type: Date, default: Date.now},
    following: [{type: Schema.Types.ObjectId, ref: 'User'}], //v2
    followers: [{type: Schema.Types.ObjectId, ref: 'User'}],//v2
    votes: [{type: Schema.Types.ObjectId, ref: 'Status'}],//v2
    unVotes: [{type: Schema.Types.ObjectId, ref: 'Status'}],//v2
})


/*UserSchema.virtual('following' , {
    ref : 'User',
    localField : 'following',
    foreignField : '_id'
});
UserSchema.virtual('followers' , {
    ref : 'User',
    localField : 'followers',
    foreignField : '_id'
});*/

UserSchema.plugin(timestamps)


UserSchema.plugin(mongoosePaginate);


module.exports = mongoose.model('User', UserSchema);

