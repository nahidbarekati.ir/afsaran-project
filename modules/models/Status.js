const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const timestamps = require('mongoose-timestamp');
const mongoosePaginate = require('mongoose-paginate');
// const bcrypt = require('bcrypt');

const StatusSchema = new Schema ({
    title : { type : String , required : true},
    cType : { type : String , required : true},
    sql_id : { type : Number , default : null},
    description : { type : String , required : true},
    enabled : { type : String , default : 1},
    type : { type : String , required : true , default : 0},
    review : { type : Boolean , default : false},
    archive : { type : Boolean , default : false},
    comment : { type : Boolean , default : true},
    downloads :  { type : Number , required : true , default : 0},
    likes :  { type : Number , required : true , default : 0},
    views :  { type : Number , required : true , default : 0},
    font :  { type : Number , default : 1},
    user_id : { type : Number , default : null},
    video_id :{ type : Number , default : null},
    videoExtension :{ type : String , default : null},
    videoOldUrl : {  type : String , default : null },//V1
    videoUrl : {  type : String , default : null },//V2
    videoType : {  type : String , default : null },
    mediaExtension : { type : String },
    thumbnail : { type : String  , default :null},
    original : { type : String , default : null},
    tags : { type : String ,default : null},
    mediaUrl : {  type : String  },
    mediaType : {  type : String , default : 0 },
    mediaName : {  type : String  },
    cat_title : {  type : String ,default : null  },
    category_old_id : {  type : Number , default : 0  },
    commentCount : { type : Number , default : 0 },
    category : {  type : Schema.Types.ObjectId  , ref : 'Category' , default :null},
    user : {  type : Schema.Types.ObjectId  , ref : 'User' },
    vote :  [{type : Schema.Types.ObjectId  , ref : 'User'}],
    unVote :  [{type : Schema.Types.ObjectId  , ref : 'User'}],
    // media : {  type : Schema.Types.ObjectId  , ref : 'Media' },
    // cat_title : { type : String },
    // videoName :{ type : Number , default : null},
    // userimage : { type : String , default : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSq06v_YeFtM-5YtnSiHIP1vqUsYva3WqKPmXNzb_tpCzwk6E6W"},

})


StatusSchema.plugin(timestamps , {
    createdAt : 'created',
    updatedAt : 'updated'
})

StatusSchema.virtual('comments' , {
    ref : 'Comment',
    localField : '_id',
    foreignField : 'status'
})

StatusSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Status' , StatusSchema);