const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');

const SlideSchema = new Schema ({

    title : { type : String , required : true},
    url : { type : String , required : true},
    type : { type : String , required : true },
    position : { type : Number , required : true },
    mediaName : { type : String , required : true},
    mediaUrl : { type : String , default : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSq06v_YeFtM-5YtnSiHIP1vqUsYva3WqKPmXNzb_tpCzwk6E6W' },
    mediaType : { type : String },
    mediaExtension : { type : String },
})

SlideSchema.plugin(timestamps)

module.exports = mongoose.model('Slide' , SlideSchema);