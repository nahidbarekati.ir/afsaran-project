const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const datetime = require("jalali-moment");

const UserSchema = new Schema ({
    type : { type : String , required : true,enum : ['facebook', 'twitter','instagram','telegram']},
    url : { type : String , default : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSq06v_YeFtM-5YtnSiHIP1vqUsYva3WqKPmXNzb_tpCzwk6E6W' },//v2
    user : [{  type : Schema.Types.ObjectId  , ref : 'User' }], //v2
})

module.exports = mongoose.model('User' , UserSchema);