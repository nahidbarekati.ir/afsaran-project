const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');
const mongoosePaginate = require('mongoose-paginate');
// const datetime = require("jalali-moment");
const UserSchema = new Schema ({

    mobile : { type : String , required : true ,  unique : true},//V2
    code: {
        otp: {type: String, default: '',},
        expiredAt: { type : String , default :null },
        use: {type: Boolean, default: false,},},
    role : {type : String , default: 'guest'},})

UserSchema.plugin(timestamps)


UserSchema.plugin(mongoosePaginate);


module.exports = mongoose.model('User2' , UserSchema);