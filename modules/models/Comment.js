const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const commentSchema = Schema({
    user : { type : Schema.Types.ObjectId , ref : 'User'},
    parent : { type : Schema.Types.ObjectId , ref : 'Comment' , default : null },
    status : { type : Schema.Types.ObjectId , ref : 'Status' , default : undefined },
    comment : { type : String , required  : true}
    // approved : { type : Boolean , default : false },
} , { timestamps : true , toJSON : { virtuals : true } });

commentSchema.plugin(mongoosePaginate);


commentSchema.virtual('replyComments' , {
    ref : 'Comment',
    localField : '_id',
    foreignField : 'parent'
});



const commentBelong = doc => {
    if(doc.status)
        return 'Status';
}

commentSchema.virtual('belongTo' , {
    ref : commentBelong,
    localField : doc => commentBelong(doc).toLowerCase(),
    foreignField : '_id',
    justOne : true
})


module.exports = mongoose.model('Comment' , commentSchema);