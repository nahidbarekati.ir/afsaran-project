const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');

const DeviceSchema = new Schema ({

    token : { type : String , required : true},
})

// DeviceSchema.plugin(timestamps)


module.exports = mongoose.model('Device' , DeviceSchema);