const Transform = require('../Transform');
const jwt = require('jsonwebtoken');

const moment = require('jalali-moment');
moment().locale('fa').format('YYYY/M/D');

module.exports = class AuthTransform extends Transform {

    transform(item,createToken = false) {

        this.createToken = createToken;
        return {
            'name' : item.name,
            'lastname' : item.lastname,
            'username' : item.username,
            // 'email' : item.email,
            'avatar' : item.avatar,
            // 'statusCount' : item.statusTotal ? item.statusTotal.status.length : 'unknown' ,
            'followingCount' : item.following.length,
            'followersCount' : item.followers.length,
            'lastlogin' : moment( item.lastLogin, '' , 'fa').fromNow(),
            ...this.showSocial(item),
            ...this.showFollows(item),
            ...this.showFollowers(item),
            ...this.showStatus(item),
            ...this.withToken(item),
        }
    }

    withToken(item) {

        if (item.token) return { token : item.token};

        if(this.createToken === true){

            let token =  jwt.sign({ user_id : item._id} , process.env.SECRET,{
                expiresIn: '110h',
                // algorithm: 'RS256'
            });
            return  {
                "access_token": token,
                "token_type": "bearer",
                "expires_in": '110h'
            }
        }
        return {};
    }



    showFollows(item) {

        const {FollowsTransform} = require('./UserTransform');

        if(this.withFollowing === true) {
            return {
                'following' : new  FollowsTransform().transformCollection(item.following),
            }
        }
        return {}
    }
    withFollowing() {
        this.withFollowing = true;
        return this;
    }

    showSocial(item) {

        if(this.withSocial === true) {
            return {
                'social' : item.social,
            }
        }
        return {}
    }
    withSocial() {
        this.withSocial = true;
        return this;
    }

    showFollowers(item) {

        const {FollowsTransform} = require('./UserTransform');

        if(this.withFollowers === true) {
            return {
                'followers' : new  FollowsTransform().transformCollection(item.followers),
            }
        }
        return {}
    }
    withFollowers() {
        this.withFollowers = true;
        return this;
    }

/*   showUserAllStatusTotal(item) {

        if(this.withUserAllStatusTotal === true) {
            const User = require(`${path.model}/User`);
            try{

               let statusObjects =  User.findOne({ _id : item._id}).select({ "status": 1}).exec();

               // console.log(statusObjects.status.length);*!/

               return {
                   'statusCount' : statusObjects.status.length ,
               }
           }catch (err){
               console.log(err)
           }
        }

    }
    withUserAllStatusTotal() {
        this.withUserAllStatusTotal = true;
        return this;
    }*/

    showStatus(item) {

        const StatusTransform = require('./StatusTransform');

        if(this.withStatus === true) {
            return {
                'status' : new  StatusTransform().transformCollection(item.status),
            }
        }
        return {}
    }
    withStatus() {
        this.withStatus = true;
        return this;
    }
}