const Transform = require('../Transform');
const jwt = require('jsonwebtoken')
module.exports = class SlideTransform extends Transform {

    transform(item,createToken = false) {
        this.createToken = createToken;
        return {
            'name' : item.name,
            'lastname' : item.lastname,
            'email' : item.email,
            'avatar' : item.avatar,
            ...this.withToken(item)
        }
    }


}