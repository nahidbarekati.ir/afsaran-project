const Transform = require('../Transform');
const MediaTransform = require('./MediaTransform');
const StatusTransform = require('./StatusTransform');

module.exports = class CategoryTransform extends Transform {
    transform(item) {
        return {
            'title' : item.title,
            'position' : item.position,
            'telegram' : item.telegram,
            'mediaUrl' : 'http://admin.news.afsaran.ir/uploads/cache/category_thumb_api/uploads/jpeg/'+item.mediaUrl,
            'mediaType' : item.mediaType,
            'mediaName' : item.mediaName,
            'mediaExtension' : item.mediaName,
            ...this.showMedia(item.media),
            ...this.showStatus(item.status)
        }
    }

    showMedia(media) {
        // console.log(media);
        if(this.withMediaStatus && media != null) {
            return {
                'media' : new MediaTransform().transform(media)
            }
        }
        return {}
    }

    withMedia() {
        this.withMediaStatus = true;
        return this;
    }

    showStatus(status) {

        if(this.withStatusCategory) {
            return {
                'status' : new StatusTransform().transform(status)
            }
        }
        return {}
    }

    withStatus() {
        this.withStatusCategory = true;
        return this;
    }

    

}