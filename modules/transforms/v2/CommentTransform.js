const Transform = require('../Transform');

const moment = require('jalali-moment');
moment().locale('fa').format('YYYY/M/D');

module.exports = class CommentTransform extends Transform {

    transform(item) {

        return {

                'id' : item._id,
                'parent' : item.parent,
                'user' : item.user,
                'comment' : item.comment,
                'status_id' : item.status,
                'createdAt' : moment(item.createdAt, '' , 'fa').fromNow(),
                'replyComments' : new ReplyCommentTransform().transformCollection(item.replyComments)

        }

    }


}

class ReplyCommentTransform extends Transform {

    transform(item) {
        return {
            'id' : item._id,
            'user' : item.user,
            'comment' : item.comment,
            'createdAt' : moment(item.createdAt, '' , 'fa').fromNow(),
        }
    }

}