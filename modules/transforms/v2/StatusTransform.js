const Transform = require('../Transform');

const moment = require('jalali-moment');
moment().locale('fa').format('YYYY/M/D');

module.exports = class StatusTransform extends Transform {


    transform(item){
        switch (item.cType){
            case ('external'):
                item.cType = 'پست بیرونی'
                break;
            case ('discussion'):
                item.cType = 'بحث جدید'
                break
            case ('idea'):
                item.cType = 'فکر جدید'
                break
        }

        if (item.type === 'image'){
            return {
                'id' : item._id ,
                'cType' : item.cType ,
                "kind": item.type,
                'title' : item.title,
                'description' : item.description,
                "review": item.review,
                "comment": item.comment,
                "downloads": item.downloads,
                "views": item.views,
                "type": item.mediaType,
                "extension": item.mediaExtension,
                "thumbnail": "http://admin.news.afsaran.ir/uploads/cache/status_thumb_api/uploads/jpeg/" + item.mediaUrl,
                "original": "http://admin.news.afsaran.ir/uploads/jpeg/" + item.mediaUrl,
                "created": moment(item.created, '' , 'fa').fromNow(),
                "tags": item.tags,
                "like": item.likes.length,
                "vote": item.vote.length,
                "unVote": item.unVote.length,
                ...this.showAuthor(item),
                "commentCount": item.commentCount,
                // "comments": item.comments,
                ...this.showComments(item),

            }
        }
        if(item.type === 'quote'){
            return {
                'id' : item._id ,
                'cType' : item.ctyp ,
                "kind": item.type,
                'title' : item.title,
                'description' : item.description,
                "review": item.review,
                "comment": item.comment,
                "downloads": item.downloads,
                "views": item.views,
                "color" : "634FFF" ,
                "created": moment(item.created, '' , 'fa').fromNow(),
                "tags": item.tags,
                "like": item.likes.length,
                "vote": item.vote.length,
                "unVote": item.unVote.length,
                ...this.showAuthor(item),
                // "comments": item.comments,
                ...this.showComments(item),

            }
        }
        if(item.type === 'video'){
            return {
                'id' : item._id ,
                'cType' : item.cType ,
                "kind": item.type,
                'title' : item.title,
                'description' : item.description,
                "review": item.review,
                "comment": item.comment,
                "downloads": item.downloads,
                "views": item.views,
                "type": item.videoType,
                "extension": item.videoExtension,
                "thumbnail": "http://admin.news.afsaran.ir/uploads/cache/status_thumb_api/uploads/jpeg/" + item.mediaUrl,
                // "original": "http://admin.news.afsaran.ir/uploads/jpeg/" + item.mediaUrl,
                "original": item.videoOldUrl == null ? 'http://localhost:3000/public/'+item.videoUrl : item.videoOldUrl,
                "created": moment(item.created, '' , 'fa').fromNow(),
                "tags": item.tags,
                "like": item.likes.length,
                "vote": item.vote.length,
                "unVote": item.unVote.length,
                "cat_id": item.category_old_id,
                ...this.showAuthor(item),
                ...this.showComments(item),
                // "comments": item.comments,

            }
        }
    }



    showAuthor(item) {
        const UserTransform = require('./AuthorTransform');
        if(this.withAuthorStatus ) {
            return {
                'author' : new UserTransform().transform(item.user)
            }
        }
        return {}
    }
    withAuthor() {
        this.withAuthorStatus = true;
        return this;
    }
    showComments(item) {
        const CommentTransform = require('./CommentTransform');
        if(this.withCommentsStatus ) {
            return {
                'comments' : new CommentTransform().transformCollection(item.comments)
            }
        }
        return {}
    }
    withComments() {
        this.withCommentsStatus = true;
        return this;
    }


}