const Transform = require('../Transform');

module.exports = class SlideTransform extends Transform {

    transform(item) {

        return {
            'title' : item.title,
            'url' : item.url,
            'caption' : item.caption,
            'step' : item.step,
        }
    }


}