const Transform = require('../Transform');
const jwt = require('jsonwebtoken');
const moment = require('jalali-moment');
moment().locale('fa').format('YYYY/M/D');

module.exports = class UserTransform extends Transform {

    transform(item,createToken = false) {
        this.createToken = createToken;
        return {
           user : {
           'id' : item.id,
           'name' : item.name,
            'lastname' : item.lastname,
            'username' : item.username,
            'email' : item.email,
           // 'lastlogin' : moment(item.lastLogin, '' , 'fa').fromNow(),
            },
            ...this.withToken(item)
        }
    }
    
    OnlyUser(item) {
        return {
            'name' : item.name,
            'lastname' : item.lastname,
            'username' : (item.name+item.lastname).toLowerCase(),
            'email' : item.email,
            'avatar' : item.avatar,
            'social' : item.social,
            'following' : item.following,
            'followers' : item.followers,
            // 'status' : item.status,
        }
    }

    withToken(item) {
        if (item.token == true ) return { token : item.token};
        // console.log(this.createToken)

        if(this.createToken == true){

            let token =  jwt.sign({ user_id : item._id} , process.env.SECRET,{
                expiresIn: '110h',
                // algorithm: 'RS256'
            })

           return {

                token:{
                    "access_token": token,
                    "token_type": "bearer",
                    "expires_in": '110h'
                }
            }

        }
        return {};
    }

}