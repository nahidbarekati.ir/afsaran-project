const Transform = require('../Transform');

const moment = require('jalali-moment');
moment().locale('fa').format('YYYY/M/D');

module.exports = class StatusTransform extends Transform {


    transform(item){
        // return item;
        if (item.type === 'image'){
            return {
                'id' : item.sql_id ,
                "kind": item.type,
                'title' : item.title,
                'description' : item.description,
                "review": item.review,
                "comment": item.comment,
                "comments": item.comments,
                "downloads": item.downloads,
                "font": item.font,
                "views": item.views,
                "user": 'کاربر '+ item.user_id,
                "userid":item.user_id,
                "userimage": item.userimage,
                "type": item.mediaType,
                "extension": item.mediaExtension,
                "thumbnail": "http://admin.news.afsaran.ir/uploads/cache/status_thumb_api/uploads/jpeg/" + item.mediaUrl,
                "original": "http://admin.news.afsaran.ir/uploads/jpeg/" + item.mediaUrl,
                "created": moment(item.created, '' , 'fa').fromNow(),
                "tags": item.tags,
                "like": item.like,
                "cat_id": item.category_old_id,
                "cat_title": item.cat_title,
                ...this.showMedia(item.media),
            }
    }
        if(item.type === 'quote'){
            return {
                'id' : item.sql_id ,
                "kind": item.type,
                'title' : item.title,
                'description' : item.description,
                "review": item.review,
                "comment": item.comment,
                "comments": item.comments,
                "downloads": item.downloads,
                "font": item.font,
                "views": item.views,
                "user": 'کاربر '+ item.user_id,
                "userid":item.user_id,
                "userimage": item.userimage,
                "color" : "634FFF" ,
                "created": moment(item.created, '' , 'fa').fromNow(),
                "tags": item.tags,
                "like": item.like,
                "cat_id": item.category_old_id,
                "cat_title": item.cat_title,
            }
        }
        if(item.type === 'video'){
            return {
                'id' : item.sql_id ,
                "kind": item.type,
                'title' : item.title,
                'description' : item.description,
                "review": item.review,
                "comment": item.comment,
                "comments": item.comments,
                "downloads": item.downloads,
                "font": item.font,
                "views": item.views,
                "user": 'کاربر '+ item.user_id,
                "userid":item.user_id,
                "userimage": item.userimage,
                // "media": item.media,
                "type": item.mediaType,
                "extension": item.mediaExtension,
                "thumbnail": "http://admin.news.afsaran.ir/uploads/cache/status_thumb_api/uploads/jpeg/" + item.mediaUrl,
                // "original": "http://admin.news.afsaran.ir/uploads/jpeg/" + item.mediaUrl,
                "original": item.videoUrl,
                "created": moment(item.created, '' , 'fa').fromNow(),
                "tags": item.tags,
                "like": item.like,
                "cat_id": item.category_old_id,
                "cat_title": item.cat_title,
            }
        }
    }

    showMedia(media) {
        // console.log(media);
        const MediaTransform = require('./MediaTransform');

        if(this.withMediaStatus && media != null) {
            return {
                'media' : new  MediaTransform().transform(media)
            }
        }
        return {}
    }
    withMedia() {
        this.withMediaStatus = true;
        return this;
    }




    showCategory(category) {
        const CategoryTransform = require('./CategoryTransform');
        if(this.withCategoryStatus && category != null) {
            return {
                // 'category' : new CategoryTransform().transform(category)
                'cat' : category
            }
        }
        return {}
    }

    withCategory() {
        this.withCategoryStatus = true;
        return this;
    }



}