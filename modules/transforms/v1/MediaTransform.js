const Transform = require('../Transform');
module.exports = class MediaTransform extends Transform {


    transform(item){
        return {
            titre : item.titre,
            url : item.url,
            type : item.type,
            extension : item.extension,
            // ...this.showStatus(item)
        }
    }

    // showStatus(item) {
    //     const StatusTransform = require('./StatusTransform')
    //     if(this.withStatusStatus) {
    //         return {
    //             'media' : new StatusTransform().transform(item.status)
    //         }
    //     }
    //     return {}
    // }
    //
    // withStatus() {
    //     this.withStatusStatus = true;
    //     return this;
    // }


}