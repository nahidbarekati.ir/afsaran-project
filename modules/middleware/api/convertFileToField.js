const User = require('./../../models/User');
const middleware = require('./../middleware');

class convertFileToField extends middleware {

    handle(req , res , next) {
        if(!req.file) {

            req.body.video = undefined;
            req.body.image = undefined;

        }
        else
        {

            if (req.file.originalname.match(/\.(mp4|movie|mkv)$/)){
                req.body.video = req.file.filename;}

            else if ( req.file.originalname.match(/\.(png|jpg|jpeg|gif)$/) ){
                req.body.image = req.file.filename;
                // console.log(req.body)
            }

        }

        next();
    }
}


module.exports = new convertFileToField();