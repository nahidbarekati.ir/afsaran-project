const multer  = require('multer')
const mkdirp  = require('mkdirp')


const imageStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        let year = new Date().getFullYear();
        let month = new Date().getMonth();
        let day = new Date().getDay();
        const dir = `./public/uploads/images/${year}/${month}/${day}`;

        mkdirp.sync(dir);
        cb(null , dir)
    },

    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix+ '_' + file.originalname )
    }
})

const imageFilter = (req , file , cb) => {
    if(file.mimetype === "image/png" || file.mimetype === "image/jpeg") {
        cb(null , true)
    } else {
        cb(null , false)
    }
}

const uploadImage = multer({
    storage : imageStorage,
    limits : {
        fileSize : 1024 * 1024 * 100
    },
    fileFilter : imageFilter,
})

module.exports = {
    uploadImage
}