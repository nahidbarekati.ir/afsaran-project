const datetime = require("jalali-moment");

const jwt = require('jsonwebtoken');
const User = require(`${path.model}/User`);

module.exports = async (req , res ,next ) =>  {
    let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.params.token;

    if(token) {
        return jwt.verify(token , process.env.SECRET , (err , decode ) => {
            // console.log(decode);

            if (!decode){
                return res.status(404).json({
                    success : false ,
                    data : 'User not found'
                });
            }

            const {user_id} = decode ;//decode.user_id
            if (user_id){
                // {$set:{lastLogin:datetime.now()}}
                 User.findByIdAndUpdate( user_id ,{'lastLogin' : datetime.now()}, (err , user) => {
                    if(err) throw err;


                    if(user) {
                        // user.token = token;
                        req.user = user;
                        // console.log(req.user);
                        next();

                    } else {
                        return res.json({
                            success : false ,
                            data : 'User not found'
                        });
                    }

                })

                 .populate({//IF WithStatus IS True

                     path: 'status',
                         // match: { key: req.params.categoryKey },
                         options: {
                             sort: { created : -1 },
                             limit: 30
                         }
                     })
                .populate('followers')
                .populate('following').exec()
            }



        })
    }

    return res.status(403).json({
        data : 'No Token Provided',
        success : false
    })
}