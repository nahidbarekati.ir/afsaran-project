const autoBind = require('auto-bind');
// const isMongoId = require('validator/lib/isMongoId');

//Model
const User = require(`${path.model}/User`);
const Media = require(`${path.model}/Media`);
const Status = require(`${path.model}/Status`);
const Category = require(`${path.model}/Category`);
const Slide = require(`${path.model}/Slide`);
const Device = require(`${path.model}/Device`);
const { check, validationResult } = require('express-validator');
const isMongoId = require('validator/lib/isMongoId');

module.exports = class Controller {

    constructor(){
        autoBind(this);

        this.model = {  User,Status,Media,Category,Device,Slide}
    }

    async validationData(req,res) {
        const result = validationResult(req);
        if (! result.isEmpty()) {
            const errors = result.array();
            const messages = [];

            errors.forEach(err => messages.push(err.msg));
            req.errors = messages;
            // req.flash('errors' , messages)
            return messages;
        }

        return true;
    }

    isMongoId(paramId) {
        if(! isMongoId(paramId))
            this.error('ای دی وارد شده صحیح نیست', 404);
    }


    error(message , status = 500) {
        let err = new Error(message);
        err.status = status;
        throw err;
    }

    slug(title) {
        return title.replace(/([^۰-۹آ-یa-z0-9]|-)+/g , "-")
    }
}

    // showValidatorErrors(req,res,errors){
    //     return res.status(422).json({
    //         message : errors.array().map( error => {
    //             return {
    //                 'field' : error.param,
    //                 'message' : error.msg,
    //             }
    //         }),
    //         success : false
    //     });
    // }

