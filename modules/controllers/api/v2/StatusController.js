const Controller = require(`${path.controller}`);
const StatusTransform = require(`${path.transform}/v2/StatusTransform`)
const { validationResult } = require('express-validator');
const Status = require(`${path.model}/Status`);
const fs = require('fs');
const path2 = require('path');
const sharp = require('sharp');



module.exports = new class StatusController extends Controller {

    index (req , res ) {

        Status.paginate({},{page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30 , populate:'media' , sort: -req.params.order  || { created  : -1} }).then( status => {

                if(status) {
                    return res.json({

                        ...new StatusTransform().withPaginate().transformCollection(status),
                        success : true
                    });
                }

                res.json({
                    message : 'Status empty',
                    success : false
                })

            })

    }

    externalStatus (req , res ) {

        // Status.find({}).limit(300).populate('media').exec( (err, status) => {
        Status.paginate({ cType : 'external' },{page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30 ,  sort: -req.params.order  || { created  : -1} }).then( status => {

            if(status) {
                return res.json({
                    ...new StatusTransform().withPaginate().transformCollection(status),
                    success : true
                });
            }

            res.json({
                message : 'Status empty',
                success : false
            })

        })

    }
    discussionStatus (req , res ) {


        // Status.find({}).limit(300).populate('media').exec( (err, status) => {
        Status.paginate({cType : 'discussion'},{page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30 , populate:'media' , sort: -req.params.order  || { created  : -1} }).then( status => {

            if(status) {
                return res.json({

                    ...new StatusTransform().withPaginate().transformCollection(status),
                    success : true
                });
            }

            res.json({
                message : 'Status empty',
                success : false
            })

        })

    }
    ideaStatus (req , res ) {


        // Status.find({}).limit(300).populate('media').exec( (err, status) => {
        Status.paginate({ cType : 'idea' },{page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30 ,  sort: -req.params.order  || { created  : -1} }).then( status => {

            if(status) {
                return res.json({
                    ...new StatusTransform().withPaginate().transformCollection(status),
                    success : true
                });
            }

            res.json({
                message : 'Status empty',
                success : false
            })

        })

    }


    //ALL QUERY GET ACTION API
    allQuery(req,res) {
        Status.paginate({},{page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30, sort: { 'createdAt'  : -1 }  || 'createdAt' , populate:'media' })
            .then(result => {

                if(result) {
                    return res.json({

                        data : new StatusTransform().withMedia().withCategory().withPaginate().transformCollection(result),
                        success : true
                    });
                }
            })
            .catch(err => console.log(err));

    }c

    //Show Single Post
    show( req , res ){
        Status.findById( req.params.id)
            .populate('media')
            .populate([
                    {
                        path : 'comments' ,
                        match : { parent : { $eq : null } } ,
                        options: { sort: { createdAt: -1 }/*, limit: 5*/ },
                        populate : [
                                        { path : 'user' , select : ['name' ,'lastname' , 'username' , 'avatar'] , } ,
                                        { path:  'replyComments' ,populate :  { path : 'user' , select : ['name' , 'lastname' , 'username' , 'avatar']  }  }
                                    ],
                    }
                    ])
            .populate('user')
            .exec((err , status) => {
                if(err) throw err;
                return res.status(200).json({
                    ...new StatusTransform().withAuthor().withComments().transform(status),
                    // data : status,
                    success : true
                });
            });


    }

    //Search Query in Data
    async search(req,res){

        let query = {};

        if(req.query.search || req.params.query)
            query.title = new RegExp(req.query.search , 'gi');

        if(req.query.type && req.query.type !== 'all')
            query.type = req.query.type;

        let status = Status.find({ ...query });


        if(req.query.order)
            status.sort({ createdAt : -1})

        status = await status.exec();

        res.json({
            data : new StatusTransform().withMedia().withCategory().transform(status),
            success : true
        })
    }

    //Search Query in Data
    async searchCaption(req,res){

        let query = {};

        if(req.query.search || req.params.query)
            query.title = new RegExp(req.query.search , 'gi');

        if(req.query.type && req.query.type !== 'all')
            query.type = req.query.type;

        let status = Status.find({ ...query });


        if(req.query.order)
            status.sort({ createdAt : -1})

        status = await status.exec();

        res.json({
            data : new StatusTransform().withMedia().withCategory().transform(status),
            success : true
        })
    }

    async searchHashtag(req,res){
        let page = parseInt(req.params.page) || parseInt(req.body.page) || 1;
        let limit = parseInt(req.params.limit) || parseInt(req.body.limit) || 10;
        let skip = ( page - 1) * limit;let total = 0;
        let term=req.params.search || req.body.search;

        if (!term || term==='#'){res.status(400).json({status:400,message:'عبارتی وارد نشده است'})}

        let checkHashtag=term.split('#');
        if (checkHashtag.length<=1) {term=`#${term}`;}
        term=term.trim();

        /*       //Get today's date using the JavaScript Date object.
        var ourDate = new Date();
        //Change it so that it is 7 days in the past.
        var pastDate = ourDate.getDate() - 150;

        ourDate.setDate(pastDate);


        where('created').gt(ourDate)
        */

        await Status.countDocuments({description:new RegExp(term , 'gi')}, function (err, count) { total = count;});
       let result=  await Status.find({description:new RegExp(term , 'gi')}).populate('user').sort('-downloads').sort('-likes').skip(skip).limit(limit);
       if (result.length<1){res.json({status:404,message:'پستی با این هشتگ وجود ندارد'})}
       return res.json({
           status:new StatusTransform().withAuthor().transformCollection(result),
           page,pages:Math.ceil(total/limit), limit, total
       });
    }

    async popular(req,res) {
        let page = parseInt(req.params.page) || parseInt(req.body.page) || 1;
        let limit = parseInt(req.params.limit) || parseInt(req.body.limit) || 10;
        let skip = (page - 1) * limit;
        let total = 0;
        await Status.countDocuments({}, function (err, count) {
            total = count;
        });


        //Get today's date using the JavaScript Date object.
        var ourDate = new Date();
        //Change it so that it is 7 days in the past.
        var pastDate = ourDate.getDate() - 150;


        ourDate.setDate(pastDate);
        let result =await Status.find({}).populate('user').sort('-downloads').sort('-likes').where('created').gt(ourDate).skip(skip).limit(limit);
        console.log(result)
        if (result) {
            return res.json({
                status: new StatusTransform().withAuthor().transformCollection(result),
                success: true,
                paginate: {
                    total: total,
                    // pages : Math.round(total / limit),
                    currentPgae: page,
                    limit: limit
                }
            })
        }
    }

    addLike(req,res){
        // var data = await pool.query("UPDATE status_table SET likes = likes + 1 WHERE id = " + req.params.id );
    }

    disLike(req,res){
        // var data = await pool.query("UPDATE status_table SET likes = likes - 1 WHERE id = " + req.params.id );

    }

    addView(req,res){
        // var data = await pool.query("UPDATE status_table SET views = views + 1 WHERE id = " + req.params.id );

    }

    async vote(req,res){

        try {
            const { action } = req.body;

            switch(action) {
                case 'vote':
                    await Promise.all([
                        Status.findByIdAndUpdate(req.body.status, { $addToSet: { votes: req.user.id }}),
                        this.model.User.findByIdAndUpdate(req.user.id , { $addToSet: { votes: req.body.status }}),
                        Status.findByIdAndUpdate(req.body.status, { $pull: { unVotes: req.user.id }}),
                        this.model.User.findByIdAndUpdate(req.user.id , { $pull: { unVotes: req.body.status }}),
                    ]);

                    break;

                case 'unvote':
                    await Promise.all([
                        Status.findByIdAndUpdate(req.body.status, { $addToSet: { unVotes: req.user.id }}),
                        this.model.User.findByIdAndUpdate(req.user.id , { $addToSet: { unVotes: req.body.status }}),
                        Status.findByIdAndUpdate(req.body.status, { $pull: { votes: req.user.id }}),
                        this.model.User.findByIdAndUpdate(req.user.id , { $pull: { votes: req.body.status }}),
                    ]);
                    break;

                default:
                    break;
            }

            res.status(200).json({
                status: 'success',
                message : 'با موفقیت همراه بود'
            });

        } catch(err) {
            console.log(err);
            res.json({ status: 'مشکلی وجود دارد' });
        };

    }


    addDownload(req,res){
        // var data = await pool.query("UPDATE status_table SET downloads = downloads + 1 WHERE id = " + req.params.id );

    }

    followed_status (req , res ) {

         try{
             this.model.User.findOne({ username : req.params.username} , /*async*/ (err , user) => {
                 if (err) {
                     return res.json('خطا: چنین کاربری یافت نشد!')
                 }

                 let page  =  parseInt(req.params.page  || req.query.page || 1);
                 let limit =  parseInt(req.params.limit || req.query.limit || 30);
                 let skip  = (parseInt(page) - 1) * limit;

                 if (user) {
                     // console.log(user.following)
                     let foll = user.following;
                     foll.push(user._id)

                     // let statusIDs = await Status.find({user: {$in: foll.toString().split(',')}}).select({"_id": 1}).exec();
                     // let total = statusIDs.length

                     let status = Status.find({
                         user: {$in: foll.toString().split(',')}
                     },  (err, status) => {
                         return res.json({
                             status: new StatusTransform().withAuthor().transformCollection(status),
                             // total: total,
                             limit: limit,
                             page: 1,
                             // pages: Math.round(total / limit),
                         })
                     }).populate('user').skip(skip).limit(limit).sort('-created').exec()

                 }else{
                     return res.json({
                         data : 'چنین کاربری یافت نشد!',
                         success : false
                     })
                 }

             })
                 .exec((err, result) => {
                     if (err)  console.log(err);
                 });

                }catch (error){
                     this.error(error)
                }

    }

    userHomeStatus(req , res ) {
        let user = req.user;
            return res.json(req.user.following._id)

            if (req.user) {
                console.log(user.following._id)

                let foll = user.following;
                foll.push(user._id)
                // console.log(foll)
                let status = Status.find({
                    user : { $in:  foll.toString().split(',') }
                }, function(err, status) {
                    return res.json({
                        status : new StatusTransform().transformCollection(status),
                    })
                }).limit(50).sort('-created')

        }
    }

    async createImageStatus(req, res) {

       try{
           let status = await this.validationData(req,res);
           if(!status) {
               if(req.file)
                   fs.unlinkSync(req.file.path);
               return res.json(status)
           }

           //Resize Image For thumbnail
           let images = this.imageResize(req.file)

           Status.create({
               title: req.body.title,
               description: req.body.description,
               type: req.body.type,
               review: false,
               thumbnail: images.thumbnail,
               original: req.body.original,
               // mediaUrl: req.body.original,
               mediaUrl: images.original,
               cat_title: req.body.cat_title,
               tags: req.body.tags,
               user: req.body.user.id,
               mediaExtension: images.ext,
           }).then(async status => {

               if (status){
                   await this.model.User.findByIdAndUpdate(req.user.id, { $addToSet: { status: status._id }});
                   return res.json({
                       message: 'پست شما با موفقیت ذخیره گردید',
                       data: status,
                       success: true
                   });
               }

           })
       }
       catch (err){
           if(req.file)
               fs.unlinkSync(req.file.path);

           return res.json({
               message: 'خطا : مشکلی در ذخیره سازی پست شما پیش آمد',
               success: false
           })
       }
}
    async createVideoStatus(req, res) {
        try{
            let status = await this.validationData(req,res);
            if(!status) {
                if(req.file)
                    fs.unlinkSync(req.file.path);
                return res.json(status)
            }

            Status.create({
                title: req.body.title,
                description: req.body.description,
                type: 'video',
                review: req.body.review,
                videoType: req.file.mimetype,
                videoUrl: this.getUrlImage(`${req.file.destination}/${req.file.filename}`),
                // videoName: req.body.videoName,
                // cat_title: req.body.cat_title,
                // original: req.body.original,
                tags: req.body.tags,
                user: req.user.id,
                videoExtension: path2.extname(req.file.originalname).substring(1),
            }).then(async status => {

                if (status){
                    await this.model.User.findByIdAndUpdate(req.user.id, { $addToSet: { status: status._id }});
                    return res.json({
                        message: 'پست ویدئویی شما با موفقیت ذخیره گردید',
                        data: status,
                        success: true
                    });
                }

            })
        }
        catch (err){
            console.log(err)
            if(req.file)
                fs.unlinkSync(req.file.path);

            return res.json({
                message: 'خطا : مشکلی در ذخیره سازی پست شما پیش آمد',
                success: false
            })
        }
    }
    async createQuoteStatus(req, res) {
        try{
            let status = await this.validationData(req,res);
            if(!status) {
                return res.json(status)
            }

            Status.create({
                title: req.body.title,
                description: req.body.description,
                type: 'quote',
                review: req.body.review,
                tags: req.body.tags,
                user: req.user.id,
            }).then(async status => {

                if (status){
                    await this.model.User.findByIdAndUpdate(req.user.id, { $addToSet: { status: status._id }});
                    return res.json({
                        message: 'پست متنی شما با موفقیت ذخیره گردید',
                        data: status,
                        success: true
                    });
                }

            })
        }
        catch (err){
            console.log(err)
            return res.json({
                message: 'خطا : مشکلی در ذخیره سازی پست شما پیش آمد',
                success: false
            })
        }
    }

    async destroyStatus(req , res  , next) {
        try {
            this.isMongoId(req.params.id);

            let status = await Status.findById(req.params.id).populate('comments').exec();
            if( ! status ) this.error('چنین پستی وجود ندارد' , 404);

            // delete comments
            status.comments.forEach(comment => comment.remove());

            // delete Images
            Object.values(status.images).forEach(image => fs.unlinkSync(`./public${image}`));

            //Remove Id From User Status
            this.model.User.findByIdAndUpdate(req.user.id, { $pull: { status: status._id }}),

            // delete courses
            status.remove();

            return res.json({
                message: 'پست شما با موفقیت حذف گردید',
                success: true
            });

        } catch (err) {
            return res.json({
                message: 'خطا : مشکلی در حذف پست مورد نظر پیش آمد',
                success: false
            });
        }
    }

    imageResize(image) {
        const imageInfo = path2.parse(image.path);
        let addresImages = {};
        addresImages['original'] = this.getUrlImage(`${image.destination}/${image.filename}`);

        const resize = size => {
            let imageName = `thumbnail-${imageInfo.name}-${size}${imageInfo.ext}`;

            addresImages['thumbnail'] = this.getUrlImage(`${image.destination}/${imageName}`);

            sharp(image.path)
                .resize(size , null)
                .toFile(`${image.destination}/${imageName}`);
        }

        [/*1080 ,*/ 720 /*, 480*/].map(resize);
        addresImages['ext'] = imageInfo.ext;

        return addresImages;
    }

    getUrlImage(dir) {
        return dir.substring(8);
    }

    milad(req,res){
        try {
            // res.json('hello')
            console.log('sadsadasdas');

            // console.log(req.get('host'))
        } catch (err) {
            // throw err;
        }
    }
}
