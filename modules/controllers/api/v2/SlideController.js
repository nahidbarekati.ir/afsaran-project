const Controller = require(`${path.controller}`);

module.exports = new class SlidesController extends Controller{

    index (req , res ) {
        this.model.Slide.find({}, (err, Slides) => {
            if (err) throw err;

            if (Slides) {
                return res.json(Slides);
            }
        });
    }
    
}
