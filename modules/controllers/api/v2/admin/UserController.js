const Controller = require(`${path.controller}`);
const { validationResult } = require('express-validator');
const {UserTransform} = require(`${path.transform}/v2/UserTransform`);

module.exports = new class UserController extends Controller {

    /**List Of Users
     * @param {*} req
     * @param {*} res
     */
    async  index(req, res) {
        let page = req.params.page || req.body.page || 1;
        let limit = req.params.limit || req.body.limit || 30;
        let OurSort = req.params.sort || req.body.sort || 'createdAt';

        let skip = (parseInt(page) - 1) * limit;

        this.model.User.find({},async (err, users) => {
            if (err) throw err;

            if (users) {
                return res.status(200).json({
                    users: new UserTransform().withFollowers().withFollowing().withSocial().transformCollection(users),
                    success : true
                });
            }
        }).populate(
            {
                path: 'status',
                // match: { key: req.params.categoryKey },
                options: {
                    sort: {created : -1 },
                    skip: skip,
                    limit: limit
                }
            }
        )
        .populate('followers')
        .populate('following')
        .exec();
    }


    /** Show Page Of Singel User By @id Param
     * @param {*} req
     * @param {*} res
     */
    async show(req, res) {

        let page = req.params.page || req.body.page || 1;
        let limit = req.params.limit || req.body.limit || 30;
        let skip = (parseInt(page) - 1) * limit;
        // console.log(req.params.username)

        try {
            this.model.User.findOne({ username : req.params.username} , async (err , user) => {

                if (err || user == null) {
                    return res.json('خطا: چنین کاربری یافت نشد!')
                }

                if (user) {

                    //Get User Status Total Count
                    user.statusTotal = await this.model.User.findOne({ _id : user._id}).select({ "status": 1}).exec();


                    return  res.json({
                        ...new UserTransform().withFollowers().withFollowing().withStatus().transform(user),
                        UserStatusPage : page,
                        UserStatusLimit : limit,
                        UserStatusTotal : user.statusTotal.status.length,
                        success : true
                    });
                }

                res.status(422).json({
                    data : 'چنین کاربری یافت نشد!',
                    success : false
                })
            }).populate(
                {
                    path: 'status',
                    // match: { key: req.params.categoryKey },
                    // match: { review : 0 },
                    options: {
                        sort: { created : -1 },
                        skip: skip,
                        limit: limit
                    }
                }
            ).populate('followers')
                .populate('following')

                .exec((err, result) => {
                    console.log(err)
                });
        }catch (error){
            this.error(error)
        }
    }


    /** Show Page Of Singel User By @id Param
     * @param {*} req
     * @param {*} res
     */
    user_status(req, res) {
        return res.json({
            data: new UserTransform().transform(req.User)
        })
    }

    /**Store a User
     * @param {*} req
     * @param {*} res
     */
    store(req, res) {

        //Validation verified

        // let newUser = new this.model.User({
        //     name : req.body.name,
        //     lastname : req.body.lastname,
        //     email : req.body.email,
        //     password : req.body.password,
        // }).save(err => {
        //     if(err) throw err;
        //     res.json('successfully User Created')
        // })


        //Second Way To store a user
        this.model.User.create({
            name: req.body.name,
            lastname: req.body.lastname,
            email: req.body.email,
            password: req.body.password,
            username: req.body.username,
        })
            .then(user => res.json(user))


    }


    /**Update a User By Id
     *
     */
    update(req, res) {


        //Validation Data

        this.model.User.findByIdAndUpdate(req.params.id,
            {title: req.body.title}
            , (err, user) => {
                if (err) throw err;
                return res.json('successfully User Updated')
            });

    }


    /**Destroy User
     * @param {*} req
     * @param {*} res
     */
    destroy(req, res) {

        this.model.User.findByIdAndRemove(req.params.id
            , (err, user) => {
                if (err) throw err;
                return res.json('successfully User Removed')
            });

    }

    upload(req, res) {
        if (req.file) {
            res.json({
                message: 'فایل شما با موفقیت آپلود گردید',
                data: {
                    imagePath: 'http://localhost:8585/' + req.file.path.replace(/\\/g, '/')
                },
                success: true
            })
        } else {
            res.json({
                message: 'خطا : فایل شما آپلود نشد',
                success: false
            })
        }
    }

    // follow(req, res) {
    //    this.model.User.find({id : req.body.user_id}).updateOne([
    //         {$addToSet: {"following": req.body.follow_id} }
    //     ]);
    //     // return res.json(req.user);
    //
    //
    //
    //      this.model.User.findByIdAndUpdate(req.body.user_id,{
    //             $addToSet: {"following": req.body.follow_id},
    //         },
    //            (err, follow) => {
    //                if (err) throw err;
    //
    //                        this.model.User.findByIdAndUpdate(req.body.follow_id,{
    //                            $addToSet: {"followers": req.body.user_id},
    //                        },
    //                            (err, follower) => {
    //                                if (err) throw err;
    //                            })
    //
    //                return res.json({
    //                    data: 'کاربر مورد نظر فالو شد',
    //                    success: true
    //                });
    //            }
    //
    //         )
    // }
    async follow(req, res) {

      const { follower, following, action } = req.body;
      try {
          switch(action) {
              case 'follow':
                  await Promise.all([
                      //$push Added more one Id in following
                      this.model.User.findByIdAndUpdate(follower, { $addToSet: { following: following }}),
                      this.model.User.findByIdAndUpdate(following, { $addToSet: { followers: follower }}),
                  ]);

                  break;

              case 'unfollow':
                  await Promise.all([
                      this.model.User.findByIdAndUpdate(follower, { $pull: { following: following }}),
                      this.model.User.findByIdAndUpdate(following, { $pull: { followers: follower }})

                  ]);
                  break;

              default:
                  break;
          }

          res.status(200).json({
              status: 'success',
              message : 'با موفقیت همراه بود'
          });

      } catch(err) {
          console.log(err);
          res.json({ status: 'مشکلی وجود دارد' });
      }

}

    topUsers(req, res) {
        this.model.User.paginate({},{page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30 , populate:'following followers' , sort: req.params.limit  || { createdAt  : -1 } }).then( users => {

            if (users) {
                return res.send(
                    new UserTransform().withFollowing().withFollowers().withPaginate().transformCollection(users),
                );
            }
            res.json({
                message : 'Users is empty',
                success : false
            })
        });


/*        this.model.User.find({}).populate('following').populate('followers').exec( (err, users) => {
            if (err) throw err;

            if (users) {
                return res.send(
                    new UserTransform().withFollowing().withFollowers().transformCollection(users),
                );
            }
        });*/

    }

    editProfile(req, res) {

        try{
            const { name,lastname,username,facebook, twitter , instagram , youtube , telegram  } = req.body;

            console.log(req.body);
            Promise.all([
                this.model.User.findByIdAndUpdate(req.user._id, { name : name ,lastname : lastname , username : username , social:  {facebook : facebook || null ,twitter : twitter || null ,youtube : youtube || null ,instagram : instagram || null ,telegram : telegram || null}})
            ]);
           return res.status(200).json({
                status : "success" ,
                message : "با موفقیت ذخیره شد"
            })
        }catch (err){
            console.log(err);
            res.json({
                status : "error" ,
                message : "مشکلی در ذخیره اطلاعات شبکه های اجتماعی رخ داد."
            })
        }


    }

    editPassword(req, res) {

        try{
            const { name,lastname,username,facebook, twitter , instagram , youtube , telegram  } = req.body;

            console.log(req.body);
            Promise.all([
                this.model.User.findByIdAndUpdate(req.user._id, { name : name ,lastname : lastname , username : username , social:  {facebook : facebook || null ,twitter : twitter || null ,youtube : youtube || null ,instagram : instagram || null ,telegram : telegram || null}})
            ]);
           return res.status(200).json({
                status : "success" ,
                message : "با موفقیت ذخیره شد"
            })
        }catch (err){
            console.log(err);
            res.json({
                status : "error" ,
                message : "مشکلی در ذخیره اطلاعات شبکه های اجتماعی رخ داد."
            })
        }

    }
   async editPasswordWithCode(req , res) {
       //Validate Response
       let validate = await this.validationData(req, res);
       if (validate.length > 0) {
           return res.json({
               message: validate[0]
           })
       }
        let password = req.body.password;
        let confirmPassword = req.body.confirmpassword;
        //let mobile = req.body.mobile;
        if (password === confirmPassword) {
            let updatedPassUser = await this.model.User.findByIdAndUpdate(req.user._id, {
                password : password
            }, {new: true});
            //console.log(req.user)
            res.json({
                message: 'پسورد شما با موفقییت تغییر کرد',
                status: 200,
                success: true
            })
        } else {
            res.json({
                message: 'پسورد با یک دیگر برابر نیستند',
                success: false
            })
        }
    }

}
