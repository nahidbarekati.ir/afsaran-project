const Controller = require(`${path.controller}`);
const StatusTransform = require(`${path.transform}/v1/StatusTransform`)
const { validationResult } = require('express-validator');


module.exports = new class StatusController extends Controller {

    index (req , res) {
    //     this.model.Status.find({} , (err, status) => {
    //         if(err) throw err;
    //         if(status) {
    //             return res.json({
    //                 data : new StatusTransform().transformCollection(status),
    //                 success : true
    //             });
    //         }
    //     }).limit(3);

        // this.model.Status.find({}).limit(300).populate('media').exec( (err, status) => {
/*
        this.model.Status.paginate({},{page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30 , populate:'media' , sort: req.params.order  || { created  : -1} }).then( status => {

                if(status) {
                    return res.json({

                        data : new StatusTransform().withPaginate().withMedia().transformCollection(status),
                        success : true
                    });
                }

                res.json({
                    message : 'Status empty',
                    success : false
                })

            })
*/
        res.json('Code Is Commented')

    }

    //ALL QUERY GET ACTION API
    /*allQuery(req, res) {
        const order = req.params.order;
        const newOrder = /!*{ self:order } ||*!/ {created : -1 } ;

        this.model.Status.paginate({},{page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30 ,sort: {created : -1 }, populate:'media'  })
            .then(status => {
                if(status) {
                    return res.json({
                        data : new StatusTransform().transformCollection(status),
                        // data :status,
                        success : true
                    });
                }
            })
            .catch(err => console.log(err))

    }*/
    allQuery(req,res) {
        this.model.Status.paginate({},{page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30, sort: { 'created'  : -1 }  || 'created' , populate:'media users' })
            .then(result => {

                if(result) {
                    return res.send(
                        // result
                         new StatusTransform().withMedia().withCategory().withPaginateV1().transformCollection(result),
                    );
                }
            })
            .catch(err => console.log(err));

    }

    //Show Single Post
    show( req , res ){
        this.model.Status.findById( req.params.id).populate('media').populate('category').exec((err , status) => {
            return res.json(status);
                if(err) throw err;
                return res.json({
                    data : new StatusTransform().transform(res,status),
                    // data : status,
                    success : true
                });
            });

/*
            this.model.Status.findById( req.params.id , (err, status) => {

                if(err) throw err;
                if(status) {
                    return res.json({
                        data : status,
                        success : true
                    });
                }
            })
*/

    }

    //Search Query in Data
    async search(req,res){

        // var limitInPage = 30 ;
        // var firistResult = limitInPage * req.params.page ;
        // var data = await pool.query("SELECT "+SelecteStatusFileds+" FROM `status_table` A Left Join status_categories B ON (B.status_id = A.id)  LEFT JOIN category_table C ON (B.category_id = C.id )  LEFT JOIN fos_user_table U ON (A.user_id = U.id  ) LEFT JOIN media_table image ON (A.media_id = image.id  ) LEFT JOIN media_table video ON (A.video_id = video.id  )  Left Join language_table L ON (L.status_id = A.id) WHERE L.id in ("+ req.params.language +")  AND  A.enabled = 1 AND LOWER(A.title) like LOWER('%" + req.params.query + "%') OR LOWER(A.tags) like LOWER('%" + req.params.query + "%')  OR LOWER(A.description) like LOWER('%" + req.params.query + "%') ORDER BY "+ req.params.order +" DESC limit "+ firistResult +" , " + limitInPage);

        let query = {};

        if(req.query.search || req.params.query)
            query.title = new RegExp(req.query.search , 'gi');

        if(req.query.type && req.query.type !== 'all')
            query.type = req.query.type;

        let status = Status.find({ ...query });


        if(req.query.order)
            status.sort({ created : -1})

        status = await status.exec();

        res.json({
            data : new StatusTransform().withMedia().withCategory().transform(status),
            success : true
        })
    }

    async  popular(req,res){

        //Get today's date using the JavaScript Date object.
        var ourDate = new Date();
        //Change it so that it is 7 days in the past.
        var pastDate = ourDate.getDate() - 150;


        ourDate.setDate(pastDate);
        this.model.Status.find({} , (err, status) => {
            if(err) throw err;
            if(status) {
                return res.json({
                    data : new StatusTransform().transformCollection(status),
                    success : true,
                })
            }
        }).where('downloads').gt(ourDate).limit(30);

    // var data = await pool.query("SELECT * FROM `status_table` WHERE created > subdate(current_date, 7 ) AND enabled = 1 ORDER BY likes DESC,downloads DESC limit 30");
/*        this.model.Status.paginate({'create' : {$gt : ourDate }},{ page:parseInt(req.params.page) || 1 , limit : parseInt(req.params.limit) || 30, sort: {'create' : {$gt : ourDate } }  || 'created' , populate:'media' })
            .then(result => {

                if(result) {
                    return res.json({

                        data : new StatusTransform().withMedia().withCategory().withPaginate().transformCollection(result),
                        success : true
                    });
                }
            })
            .catch(err => console.log(err));*/
    }

    addLike(req,res){
        // var data = await pool.query("UPDATE status_table SET likes = likes + 1 WHERE id = " + req.params.id );
    }

    disLike(req,res){
        // var data = await pool.query("UPDATE status_table SET likes = likes - 1 WHERE id = " + req.params.id );

    }

    addView(req,res){
        // var data = await pool.query("UPDATE status_table SET views = views + 1 WHERE id = " + req.params.id );

    }

    addDownload(req,res){
        // var data = await pool.query("UPDATE status_table SET downloads = downloads + 1 WHERE id = " + req.params.id );

    }

    getDataByCategory(req,res){
        // var limitInPage = 30 ;
        // var firistResult = limitInPage * req.params.page ;
        //
        // if (req.params.language == 0){
        //     var data = await pool.query("SELECT "+SelecteStatusFileds+" FROM `status_table` A Left Join status_categories B ON (B.status_id = A.id)  LEFT JOIN category_table C ON (B.category_id = C.id )  LEFT JOIN fos_user_table U ON (A.user_id = U.id  ) LEFT JOIN media_table image ON (A.media_id = image.id  ) LEFT JOIN media_table video ON (A.video_id = video.id  )  WHERE category_id  = "+ req.params.category +" AND A.enabled = 1 ORDER BY "+ req.params.order +" DESC limit "+ firistResult +" , " + limitInPage);
        // }else {
        //     var data = await pool.query("SELECT "+SelecteStatusFileds+" FROM `status_table` A Left Join status_categories B ON (B.status_id = A.id)  LEFT JOIN category_table C ON (B.category_id = C.id )  LEFT JOIN fos_user_table U ON (A.user_id = U.id  ) LEFT JOIN media_table image ON (A.media_id = image.id  ) LEFT JOIN media_table video ON (A.video_id = video.id  )  Left Join language_table L ON (L.status_id = A.id) WHERE L.id in ("+ req.params.language +") AND category_id  = "+ req.params.category +" AND A.enabled = 1 ORDER BY "+ req.params.order +" DESC limit "+ firistResult +" , " + limitInPage);
        // }
    }


    getDataByUser(req,res){
        // var limitInPage = 30 ;
        // var firistResult = limitInPage * req.params.page ;
        // var data = await pool.query("SELECT "+SelecteStatusFileds+" FROM `status_table` A Left Join status_categories B ON (B.status_id = A.id)  LEFT JOIN category_table C ON (B.category_id = C.id )  LEFT JOIN fos_user_table U ON (A.user_id = U.id  ) LEFT JOIN media_table image ON (A.media_id = image.id  ) LEFT JOIN media_table video ON (A.video_id = video.id  )  Left Join language_table L ON (L.status_id = A.id) WHERE L.id in ("+ req.params.language +") AND A.user_id ="+ req.params.user +" AND A.enabled = 1 ORDER BY "+ req.params.order +" DESC , A.id ASC limit "+ firistResult +" , " + limitInPage );

    }


}
