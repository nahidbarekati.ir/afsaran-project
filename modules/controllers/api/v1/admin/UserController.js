const Controller = require(`${path.controller}`);
const { validationResult } = require('express-validator');
const UserTransform = require(`${path.transform}/v1/UserTransform`);

module.exports = new class UserController extends Controller {

    /**List Of Users
     * @param {*} req
     * @param {*} res
     */
    index(req, res) {

        this.model.User.find({}, (err, users) => {
            if (err) throw err;

            if (users) {
                // return res.json(users);
                return res.send(
                    // result
                    new UserTransform().transformCollection(users,false),
                );
            }
        });

    }


    /** Show Page Of Singel User By @id Param
     * @param {*} req
     * @param {*} res
     */
    show(req, res) {

            // return res.json({
            //     data : new UserTransform().transform(req.User)
            // })
        this.model.User.findById(req.params.id , (err , user) => {
            if (err) {
            return res.json('خطا: چنین کاربری یافت نشد!')
            }

            if(user) {
                return res.json({
                    data : new UserTransform().transform(user),
                    success : true
                })
            }

            res.json({
                data : 'چنین کاربری یافت نشد!',
                success : false
            })
        })
    }


    /**Store a User
     * @param {*} req
     * @param {*} res
     */
    store(req, res) {

            //Validation verified

            // let newUser = new this.model.User({
            //     name : req.body.name,
            //     lastname : req.body.lastname,
            //     email : req.body.email,
            //     password : req.body.password,
            // }).save(err => {
            //     if(err) throw err;
            //     res.json('successfully User Created')
            // })

            //Second Way To store a user
            this.model.User.create({
                name: req.body.name,
                lastname: req.body.lastname,
                email: req.body.email,
                password: req.body.password,
            }).then(user => res.json(user))


    }

    /**Update a User By Id
     *
     */
    update(req, res) {


        //Validation Data

        this.model.User.findByIdAndUpdate(req.params.id,
            { title: req.body.title }
            , (err, user) => {
                if (err) throw err;
                return res.json('successfully User Updated')
            });

    }


    /**Destroy User
     * @param {*} req
     * @param {*} res
     */
    destroy(req, res) {

        this.model.User.findByIdAndRemove(req.params.id
            , (err, user) => {
                if (err) throw err;
                return res.json('successfully User Removed')
            });

    }


    upload(req,res){
        if(req.file) {
            res.json({
                message : 'فایل شما با موفقیت آپلود گردید',
                data : {
                    imagePath : 'http://localhost:3000/' + req.file.path.replace(/\\/g , '/')
                },
                success : true
            })
        } else {
            res.json({
                message : 'خطا : فایل شما آپلود نشد',
                success : false
            })
        }
    }




}
