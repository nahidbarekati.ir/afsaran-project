const Controller = require(`${path.controller}`);
const CategoryTransform = require(`${path.transform}/v1/CategoryTransform`);

module.exports = new class CategoryController extends Controller {

    index(req , res) {
        this.model.Category.find({}).populate('media').populate('status').exec((err , categories) => {
            if(err) throw err;
            // console.log(categories)
           // res.json(categories)
            if(categories) {
                return res.json({
                    data : new CategoryTransform().withStatus().withMedia().transformCollection(categories),
                    success : true
                });
            }

            res.json({
                message : 'Status empty',
                success : false
            })
        })
        // const page = req.query.page || 1;
        // console.log(this.model.Category);

        // this.model.Category.find({}).populate('milad')
        //     .then(milad => {
        //         res.json(new CategoryTransform().withMedia().transformCollection(milad),)
        //     })
        //     .catch(err => console.log(err));
    }

/*
    index(req , res) {

        // var data = await pool.query("SELECT A.id,A.title,A.telegram,CONCAT('http://admin.news.afsaran.ir/uploads/cache/category_thumb_api/uploads/jpeg/', M.url) as image FROM  category_table A LEFT JOIN media_table M ON (M.id = A.media_id )  ORDER BY position ASC " );
        this.model.Category.find({}).populate('status').exec( (err, categories) => {

            if(err) throw err;

            if(categories) {
                return res.json({
                    data : new CategoryTransform().showStatus().transformCollection(categories),
                    success : true
                });
            }

            res.json({
                message : 'Category empty',
                success : false
            })
        })
    }
*/
}