const express = require('express');
const router = express.Router();

// Controllers
const  {api : ControllerApi } = path.controllers;
const CommentController = require(`${ControllerApi}/v2/CommentController.js`);

//Validator
const commentValidator = require('./../../../Validators/v2/commentValidator')


//Middlewares
const apiAuth = require('./../../../middleware/api/v2/apiAuth');

router.get('/comments/approved' , /*gate.can('show-approved-comments') , */CommentController.approved);
router.get('/comments' ,/* gate.can('show-comments') ,*/ CommentController.index);
router.put('/comments/:id/approved' , CommentController.update );
router.delete('/comments/:id' , CommentController.destroy);

router.post('/comment' , apiAuth ,commentValidator.handle() , CommentController.comment);




module.exports = router;