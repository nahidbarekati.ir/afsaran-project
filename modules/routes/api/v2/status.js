const express = require('express');
const router = express.Router();
const  { api : ControllerApi } = path.controllers;

//Controllers
// const StatusController = require(`${ControllerApi}/v2/StatusController`);
const StatusController = require('./../../../controllers/api/v2/StatusController');

//Helpers
const uploadImage  = require('../../../helpers/UploadImage');
const uploadVideo  = require('../../../helpers/UploadVideo');

//Middlewares
const convertFileToField = require('./../../../middleware/api/convertFileToField')
const apiAuth = require('./../../../middleware/api/v2/apiAuth');

//Validator
const statusImageValidator = require('./../../../Validators/v2/statusImageValidator')
const statusVideoValidator = require('./../../../Validators/v2/statusVideoValidator')


//ALL Status Show
router.get('/status/:page?/:order?/:limit?' , StatusController.index);

//External Status Show
router.get('/status-external/:page?/:order?/:limit?' , StatusController.externalStatus);

//Idea Status Show
router.get('/status-idea/:page?/:order?/:limit?' , StatusController.ideaStatus);

//Discussion Status Show
router.get('/status-discussion/:page?/:order?/:limit?' , StatusController.discussionStatus);

/*//ALL QUERY GET ACTION API
router.get('/all_status/:page?/:order?/:limit?' , StatusController.allQuery);*/

//Show Single Status By id
router.get('/single_status/:id' , StatusController.show);

//GET Search data By Query
router.get("/status-search-query/:page/:order/:query/", StatusController.search);


//GET Search Caption
router.get("/status-search-caption/:page/:order/:query/", StatusController.searchCaption);

//GET Search Hashtag
router.get("/status-search-hashtag/:page?/:order?/:limit?", StatusController.searchHashtag);

//GET POPULAR STATUS OVER THE PAST 7 DAYS
router.get('/popular/status/:page?/:limit?', StatusController.popular);

//Increment Like
router.get('/add/like/:id',StatusController.addLike);

//Decrement Like
router.get('/delete/like/:id', StatusController.disLike);

//Increment Views
router.get('/add/views/:id', StatusController.addView);

//Increment Download (share)
router.get('/add/downloads/:id', StatusController.addDownload);

//Status Published By Followed Users
router.get('/followed/status/:username/:page?/:limit?' , StatusController.followed_status);
// router.get('/followed/status/' ,apiAuth, StatusController.userHomeStatus);

//Store The Status Post
router.post('/status/create/image', apiAuth,uploadImage.single('image'),convertFileToField.handle,statusImageValidator.handle(),StatusController.createImageStatus);
router.post('/status/create/video', apiAuth,uploadVideo.single('video'),convertFileToField.handle,statusVideoValidator.handle(),StatusController.createVideoStatus);
router.post('/status/create/quote', apiAuth,statusVideoValidator.handle(),StatusController.createQuoteStatus);

//Destroy the status Post
router.delete('/delete/status/:id' ,apiAuth ,StatusController.destroyStatus);


//Increment Views
router.post('/add/vote/',apiAuth, StatusController.vote);

// router.get('/milad', StatusController.milad );

module.exports = router;