const express = require('express');
const router = express.Router();
const  {api : ControllerApi } = path.controllers;
const SlideController = require(`${ControllerApi}/v2/SlideController`);



//GET All Slides
router.get("/slides", SlideController.index.bind(SlideController));


module.exports = router;