const express = require('express');
const router = express.Router();

// Controllers
const  {api : ControllerApi } = path.controllers;

const HomeController = require(`${ControllerApi}/v2/HomeController`);


router.get('/' , HomeController.index);
router.get('/version' , HomeController.version);
router.get('/version/check/:code', HomeController.version);


module.exports = router;