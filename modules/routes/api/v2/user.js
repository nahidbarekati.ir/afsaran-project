const express = require('express');
const router = express.Router();
const validator = require("../../../Validators/validators");

const  {api : ControllerApi } = path.controllers;
//validator
const CreateLoginCodeValidator = require('./../../../Validators/v2/CreateLoginCodeValidator')
const password = require('./../../../Validators/v2/password')

//Middleware
const apiAuth = require('./../../../middleware/api/v2/apiAuth');

const { uploadImage } = require('../../../middleware/api/UploadMiddleware');


const AuthController = require(`${ControllerApi}/v2/AuthController`);

// AdminControllers
const UserController = require(`${ControllerApi}/v2/admin/UserController`);


/********************************************************
 * USERS METHODS START
 ********************/

//Routes With Model Binding
const AdminRouter = express.Router();

//Get All Users
// router.get('/users/page?/limit?' ,UserController.index.bind(UserController));

router.post('/login-send-code' ,CreateLoginCodeValidator.handle(), AuthController.createCode.bind(AuthController));
router.post('/login-mobile' , AuthController.loginWithMobile.bind(AuthController));
router.post('/login-mobile-forget-password' , AuthController.loginWithMobileForgetPass.bind(AuthController));
router.post('/new-password' ,apiAuth,password.handle(), UserController.editPasswordWithCode.bind(UserController));

//Create A user By Post Request
router.post('/users', validator.validate(validator.createUser),UserController.store.bind(UserController));

//Get All Users
router.get('/users/:order?/:page?/:limit?',UserController.index.bind(UserController));

//Get User By Id
router.get('/user/:username/:page?/:limit?/' , UserController.show.bind(UserController));

// router.get('/user/status/:id?' , UserController.user_status.bind(UserController));

//Update A User By Id
router.put('/user/:id' , UserController.update.bind(UserController));

//Delete A User Bt Id
router.delete('/user/:id' , UserController.destroy.bind(UserController) );

//Authentication
router.post('/login' , validator.validate(validator.loginUser),AuthController.login.bind(AuthController));


router.post('/register' ,validator.validate(validator.createUser) , AuthController.register.bind(AuthController));


router.post('/user/image' ,apiAuth, uploadImage.single('avatar'), UserController.upload.bind(UserController));


router.post('/user/follow',apiAuth, validator.validate(validator.followUser), UserController.follow.bind(UserController));


router.all('/get_top_users/:order?/:page?/:limit?' ,UserController.topUsers.bind(UserController));

//Authenticated By Token
router.get('/user' ,apiAuth, AuthController.token.bind(AuthController));

router.post('/user/edit/profile',apiAuth,  /*validator.validate(validator.createSocial),*/UserController.editProfile.bind(UserController));

/********************0
 * USERS METHODS END
 ********************************************************/



//Set Prefix '/admin' For AdminRouter Group
router.use('/admin',AdminRouter)


module.exports = router;