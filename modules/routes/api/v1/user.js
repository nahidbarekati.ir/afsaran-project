const express = require('express');
const router = express.Router();
const validator = require("../../../Validators/validators");

const  {api : ControllerApi } = path.controllers;


//Middleware
const apiAuth = require('./../../../middleware/api/v2/apiAuth');
const { uploadImage } = require('../../../middleware/api/UploadMiddleware');


const AuthController = require(`${ControllerApi}/v1/AuthController`);

// AdminControllers
const UserController = require(`${ControllerApi}/v1/admin/UserController`);


/********************************************************
 * USERS METHODS START
 ********************/

//Routes With Model Binding
const AdminRouter = express.Router();


//Get All Users
AdminRouter.get('/users' ,UserController.index.bind(UserController));

//Get User By Id
AdminRouter.get('/users/:id' ,validator.validate(validator.showUser), UserController.show.bind(UserController));

//Create A user By Post Request
AdminRouter.post('/users', validator.validate(validator.createUser),UserController.store.bind(UserController));

//Update A User By Id
AdminRouter.put('/users/:id' , UserController.update.bind(UserController));

//Delete A User Bt Id
AdminRouter.delete('/users/:id' , UserController.destroy.bind(UserController) );

//Authentication
router.post('/login' , validator.validate(validator.loginUser),AuthController.login.bind(AuthController));
router.post('/register' ,validator.validate(validator.createUser) , AuthController.register.bind(AuthController));
router.post('/user/image' ,apiAuth, uploadImage.single('avatar'), UserController.upload.bind(UserController));

router.get('/user' ,apiAuth, AuthController.token.bind(AuthController));

/********************
 * USERS METHODS END
 ********************************************************/


//Set Prefix '/admin' For AdminRouter Group
router.use('/admin',AdminRouter)


module.exports = router;