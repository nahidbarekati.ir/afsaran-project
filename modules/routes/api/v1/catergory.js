const express = require('express');
const router = express.Router();

// Controllers
const  {api : ControllerApi } = path.controllers;
const CategoryController = require(`${ControllerApi}/v1/CategoryController`);


//GET All Categories
router.get("/categories", CategoryController.index.bind(CategoryController));

module.exports = router;