const express = require('express');
const router = express.Router();

//Routers Call
const HomeRouter = require('./home');
const StatusRouter = require('./status');
const UserRouter = require('./user');
const SlideRouter = require('./slide');
const CategoryRouter = require('./catergory');


//Status Routers
router.use(StatusRouter);

//User Routers
router.use(UserRouter);

//Slider Routers
router.use(SlideRouter);

//Slider Routers
router.use(CategoryRouter);

//Home Routers
router.use(HomeRouter);


//REGISTER DEVICE BY TOKEN
// router.get('/device/:token', AuthController.device.bind(AuthController));


module.exports = router;