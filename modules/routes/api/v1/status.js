const express = require('express');
const router = express.Router();

const  {api : ControllerApi } = path.controllers;

// const StatusController = require(`${ControllerApi}/v1/StatusController`);
const StatusController = require('../../../controllers/api/v1/StatusController');

const Auth = require('./../../../middleware/api/v1/apiAuth');


//ALL Status Show
router.get('/status' , StatusController.index.bind(StatusController));

//ALL QUERY GET ACTION API
// router.get('/status/all/:page/:order/:limit' , StatusController.allQuery.bind(StatusController));
// router.get('/status/all/:page/:language/:order/:token' ,apiAuth, StatusController.allQuery.bind(StatusController));
router.get('/status/all/:page/:order/:language/:token' ,Auth, StatusController.allQuery.bind(StatusController));

//Show Single Status By id
router.get('/status/:id' ,Auth, StatusController.show.bind(StatusController));

//GET data By Category ID
router.get('/status/category/:page/:order/:category/',Auth, StatusController.getDataByCategory.bind(StatusController));

//GET Search data By Query
router.get("/status/query/:page/:order/:query/", StatusController.search.bind(StatusController));

//GET data By User ID
router.get("/status/user/:page/:order/:user/", StatusController.getDataByUser.bind(StatusController));

//GET POPULAR STATUS OVER THE PAST 7 DAYS
router.get('/category/popular/:token',Auth, StatusController.popular.bind(StatusController));


//Increment Like
router.get('/add/like/:id',StatusController.addLike.bind(StatusController));

//Decrement Like
router.get('/delete/like/:id', StatusController.disLike.bind(StatusController));

//Increment Views
router.get('/add/views/:id', StatusController.addView.bind(StatusController));

//Increme Download (share)
router.get('/add/downloads/:id', StatusController.addDownload.bind(StatusController));



module.exports = router;