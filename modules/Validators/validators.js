// const { check } = require('express-validator/check');
const { body ,param, validationResult} = require('express-validator');

exports.createUser = [

    body('name', 'فیلد نام الزامی می باشد').not().isEmpty(),
    body('lastname', 'فیلد نام خانوادگی الزامی می باشد').not().isEmpty(),
    body('username', 'فیلد کلمه عبور الزامی می باشد').not().isEmpty(),
    body('username', 'فیلد کلمه عبور الزامی می باشد').escape(),
    body('email', 'لطفا یک ایمیل معتبر وارد نمایید').isEmail(),
        body('password', 'فیلد کلمه عبور الزامی می باشد').not().isEmpty(),
        body('password', 'کلمه عبور نباید کمتر از 5 کاراکتر باشد').isLength({ min: 5 }),
        // body('password', 'کلمه عبور نباید کمتر از 5 کاراکتر باشد').not().isLength({ min: 5 }),
        ];

exports.loginUser = [
    body('email', 'لطفا یک ایمیل معتبر وارد نمایید').isEmail().not().isEmpty(),
    body('password', 'فیلد کلمه عبور الزامی می باشد').not().isEmpty(),
];

exports.followUser = [
    body('follower', 'آی دی کاربر دنبال کننده نباید خالی از مقدار باشد').not().isEmpty(),
    // body('user_id', 'لطفا یک آی دی معتبر برای کاربر وارد نمایید').isMongoId(),
    body('following', 'لطفا یک آی دی معتبر برای دنبال شونده وارد نمایید').isMongoId(),
    body('action', 'لطفا مقدار اکشن را مشخص نمایید.follow/unfollow').not().isEmpty(),
];
// exports.createSocial = [
//     body('type', 'لطفا شبکه اجتماعی مورد نظر را وارد نمایید').not().isEmpty(),
//     // body('user_id', 'لطفا یک آی دی معتبر برای کاربر وارد نمایید').isMongoId(),
//     body('type', 'لطفا یک آی دی معتبر برای دنبال شونده وارد نمایید').isMongoId(),
//     body('action', 'لطفا مقدار اکشن را مشخص نمایید.follow/unfollow').not().isEmpty(),
// ];

exports.showUser = [
        param('id' , 'آی دی وارد شده صحیح نیست.').isMongoId()
]




        // can be reused by many routes
exports.validate = validations => {
        return async (req, res, next) => {
          await Promise.all(validations.map(validation => validation.run(req)));
      
          const errors = validationResult(req);
          if (errors.isEmpty()) {
            return next();
          }
      
        //** Validation hass Error
        //   return res.status(422).json({ errors: errors.array() });
          return res.status(422).json({ 
                message: errors.array().map(error => {
                        return {
                            'field': error.param,
                            'message': error.msg,
                        }
                    }),
                    success: false
           });
        };
      };
