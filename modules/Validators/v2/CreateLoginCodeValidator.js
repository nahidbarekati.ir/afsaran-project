const validator = require('./validator');
const { body } = require('express-validator');
const User = require('../../models/User');

class createLoginCodeValidator extends validator {
    
    handle() {
        return [

            body('mobile')
                .not().isEmpty().withMessage('mobile: ورود شماره موبایل الزامی است.')
                .isLength({ min : 11})
                .withMessage(' شماره موبایل نمی تواند کمتر از 11 رقم باشد')
                .isLength({ max : 11})
                .withMessage(' شماره موبایل نمی تواند بیشتر از 11 رقم باشد')
                .custom(async (value , { req }) => {
                  if(!this.phoneNumber(req.body.mobile)){
                      throw new Error("شماره موبایل وارد شده صحیح نمی باشد")
                  }
                }),

                // .custom(async (value , { req }) => {
                //     let user = await User.findOne({mobile:req.body.mobile} );
                //     if(!user) {
                //         throw new Error('User Not Founded with this Number')
                //     }
                // }),

        ]
    }
    phoneNumber(number){
        let pattern = /^(0( |-)?|\+98( |-)?|098( |-)?)9((1|0)[0-9]|3[0-9]|2[0-9]|9[0-9])( |-)?[0-9]{3}( |-)?[0-9]{4}$/;

        return number.match(pattern)
    }

}

module.exports = new createLoginCodeValidator();