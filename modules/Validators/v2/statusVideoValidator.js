const validator = require('./validator');
const { body } = require('express-validator');
const Status = require('../../models/Status');
const path = require('path');

class statusValidator extends validator {
    
    handle(req) {
        return [
            body('title')
                .not().isEmpty()
                .withMessage('وارد کردن عنوان الزامی است')
                .isLength({ min : 5 })
                .withMessage('عنوان نمیتواند کمتر از 5 کاراکتر باشد')
                .custom(async (value , { req }) => {
                    if(req.query._method === 'put') {
                        let status = await Status.findById(req.params.id);
                        if(status.title === value) return;
                    }
                    // let status = await Status.findOne({ slug : this.slug(value) });
                    // if(status) {
                    //     throw new Error('چنین دوره ای با این عنوان قبلا در سایت قرار داد شده است')
                    // }
                }),

            body('image')
                .custom(async (value , { req }) => {
                    // console.log(req.body)
                    if(req.query._method === 'put' && value === undefined) return;
                    if(! value)
                        throw new Error('وارد کردن ویدئو الزامی است');

                    let fileExt = [ '.mp4' , '.mkv'];
                    if(! fileExt.includes(path.extname(value)))
                        throw new Error('پسوند فایل وارد شده از پسوندهای ویدئویی نیست')
                }),

            /*
                        check('type')
                            .not().isEmpty()
                            .withMessage('فیلد نوع دوره نمیتواند خالی بماند'),

                        check('body')
                            .isLength({ min : 20 }).not().isEmpty()
                            .withMessage('متن دوره نمیتواند کمتر از 20 کاراکتر باشد'),

                        check('price')
                            .not().isEmpty()
                            .withMessage('قیمت دوره نمیتواند خالی بماند'),

                        check('tags')
                            .not().isEmpty()
                            .withMessage('فیلد تگ نمیتواند خالی بماند'),*/
        ]
    }

    
    slug(title) {
        return title.replace(/([^۰-۹آ-یa-z0-9]|-)+/g , "-")
    }
}

module.exports = new statusValidator();