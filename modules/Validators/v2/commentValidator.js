const validator = require('./validator');
const { body } = require('express-validator');
const path = require('path');
const Comment = require('../../models/Comment');
const Status = require('../../models/Status');
const isMongoId = require('validator/lib/isMongoId');

class commentValidator extends validator {
    
    handle() {
        return [

            body('comment')
                .not().isEmpty().withMessage('comment: شما نمیتوانید دیدگاهی بدون متن ثبت نمایید')
                .isLength({ min : 1 , max : 160 })
                .withMessage('comment: متن دیدگاه نمی تواند کمتر از 10 کاراکتر باشد'),



            body('status')
                .not().isEmpty()
                .withMessage('status: آی دی پست مورد نظر را مشخص نمایید')

                .isMongoId()
                .withMessage('status: لطفا یک آی پست معتبر وارد نمایید')

                .custom(async (value , { req }) => {
                    let status = await Status.findById( req.body.status );
                    if(!status) {
                        throw new Error('چنین پستی با این مشخصات وجود ندارد.')
                    }
                 }),

            body('parent')

                .custom(async (value , { req }) => {

                    if (req.body.parent || req.body.parent === '' ){
                        if(! isMongoId(req.body.parent) || req.body.parent === ''){
                            throw new Error('parent: فرمت آی دی وارد شده صحیح نیست. ');}

                        let comment = await Comment.findById( req.body.parent );

                        if(!comment) {
                            throw new Error('parent: چنین دیدگاهی با این مشخصات وجود ندارد.')
                        }
                    }

                }).withMessage('parent: چنین دیدگاهی با این مشخصات وجود ندارد')

        ]
    }

}

module.exports = new commentValidator();