const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require("mongoose");
global.path = require('./modules/paths')
const env = require('dotenv').config()

/*var multer = require('multer');
var upload = multer();*/

//connect To DB
mongoose.connect(`mongodb://127.0.0.1:27017/${process.env.DATABASE }`, {useUnifiedTopology: true, useNewUrlParser: true,useFindAndModify: false } );
// mongoose.connect('mongodb://user:password@sample.com:port/dbname' , {useUnifiedTopology: true, useNewUrlParser: true })
// mongoose.connect('mongodb://admin:admin@127.0.0.1:27017/nahidDb' , {useUnifiedTopology: true, useNewUrlParser: true })
// mongoose.connect(`mongodb://<USER_NAME>:${encodeURIComponent('<P@SS>')}@<HOST>:<PORT>/<DB_NAME>`)
// mongoose.connect(`mongodb://admin:${encodeURIComponent('admin')}@127.0.0.1:27017/nahidDb`)
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);

app.use(bodyParser.urlencoded({extended : false}));
app.use(bodyParser.json({type : 'application/json'}))

/*app.post('/endpoint', function (req, res) {
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) {
        // fields fields fields
    });
})*/

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader("Access-Control-Allow-Origin", "*");
    next();
});

const webRouter = require('./modules/routes/web');
const apiRouter = require('./modules/routes/api');


app.use('/' , webRouter);
app.use('/api' , apiRouter);
app.use('/public' , express.static('public'));



app.listen(process.env.PORT , () => {
    console.log(`server Running at Port ${process.env.PORT}`)
})